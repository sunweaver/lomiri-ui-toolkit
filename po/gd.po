# Gaelic; Scottish translation for lomiri-ui-toolkit
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-ui-toolkit package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-toolkit\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2015-11-05 10:05+0100\n"
"PO-Revision-Date: 2014-09-30 16:22+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Gaelic; Scottish <gd@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : "
"(n > 2 && n < 20) ? 2 : 3;\n"
"X-Launchpad-Export-Date: 2017-04-05 07:13+0000\n"
"X-Generator: Launchpad (build 18335)\n"

#: Lomiri/Components/1.2/TextInputPopover.qml:29
#: Lomiri/Components/1.3/TextInputPopover.qml:29
msgid "Select All"
msgstr "Tagh na h-uile"

#: Lomiri/Components/1.2/TextInputPopover.qml:36
#: Lomiri/Components/1.3/TextInputPopover.qml:36
msgid "Cut"
msgstr "Gearr às"

#: Lomiri/Components/1.2/TextInputPopover.qml:48
#: Lomiri/Components/1.3/TextInputPopover.qml:48
msgid "Copy"
msgstr "Dèan lethbhreac"

#: Lomiri/Components/1.2/TextInputPopover.qml:57
#: Lomiri/Components/1.3/TextInputPopover.qml:57
msgid "Paste"
msgstr "Cuir ann"

#: Lomiri/Components/1.2/ToolbarItems.qml:143
#: Lomiri/Components/1.3/ToolbarItems.qml:143
msgid "Back"
msgstr "Air ais"

#: Lomiri/Components/ListItems/1.2/Empty.qml:398
#: Lomiri/Components/ListItems/1.3/Empty.qml:398
msgid "Delete"
msgstr "Sguab às"

#: Lomiri/Components/plugin/adapters/dbuspropertywatcher_p.cpp:51
msgid "No service/path specified"
msgstr "Cha deach seirbheis/slighe a shònrachadh"

#: Lomiri/Components/plugin/adapters/dbuspropertywatcher_p.cpp:69
#, qt-format
msgid "Invalid bus type: %1."
msgstr "Bus de sheòrsa mì-dhligheach: %1"

#. TRANSLATORS: Time based "this is happening/happened now"
#: Lomiri/Components/plugin/i18n.cpp:268
msgid "Now"
msgstr "An-dràsta"

#: Lomiri/Components/plugin/i18n.cpp:275
#, qt-format
msgid "%1 minute ago"
msgid_plural "%1 minutes ago"
msgstr[0] "%1 mhionaid air ais"
msgstr[1] "%1 mhionaid air ais"
msgstr[2] "%1 mionaidean air ais"
msgstr[3] "%1 mionaid air ais"

#: Lomiri/Components/plugin/i18n.cpp:277
#, qt-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 mhionaid"
msgstr[1] "%1 mhionaid"
msgstr[2] "%1 mionaidean"
msgstr[3] "%1 mionaid"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:284
msgid "h:mm ap"
msgstr "h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:287
msgid "HH:mm"
msgstr "HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:293
msgid "'Yesterday 'h:mm ap"
msgstr "'An-dè 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:296
msgid "'Yesterday 'HH:mm"
msgstr "'An-dè 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:302
msgid "'Tomorrow 'h:mm ap"
msgstr "'A-màireach 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:305
msgid "'Tomorrow 'HH:mm"
msgstr "'A-màireach 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:312
msgid "ddd' 'h:mm ap"
msgstr "ddd' 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:315
msgid "ddd' 'HH:mm"
msgstr "ddd' 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:322
msgid "ddd d MMM' 'h:mm ap"
msgstr "ddd d MMM' 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:325
msgid "ddd d MMM' 'HH:mm"
msgstr "ddd d MMM' 'HH:mm"

#: Lomiri/Components/plugin/privates/listitemdragarea.cpp:122
msgid ""
"ListView has no ViewItems.dragUpdated() signal handler implemented. No "
"dragging will be possible."
msgstr ""
"Cha deach làimhsichear siognail ViewItems.dragUpdated() a chur an sàs aig "
"ListView. Chan urrainnear slaodadh a dhèanamh."

#: Lomiri/Components/plugin/statesaverbackend_p.cpp:176
#, qt-format
msgid ""
"property \"%1\" of object %2 has type %3 and cannot be set to value \"%4\" "
"of type %5"
msgstr ""
"tha roghainn \"%1\" an oibseict %2 dhen t-seòrsa %3 agus chan urrainn dhut "
"an luach \"%4\" dhen t-seòrsa %5 a shònrachadh dha"

#: Lomiri/Components/plugin/statesaverbackend_p.cpp:185
#, qt-format
msgid "property \"%1\" does not exist or is not writable for object %2"
msgstr ""
"chan eil an roghainn \"%1\" ann no cha ghabh a sgrìobhadh airson an oibseict "
"%2"

#: Lomiri/Components/plugin/ucalarm.cpp:41
#: Lomiri/Components/plugin/ucalarm.cpp:643
msgid "Alarm"
msgstr "Caismeachd"

#: Lomiri/Components/plugin/ucalarm.cpp:635
#: Lomiri/Components/plugin/ucalarm.cpp:667
msgid "Alarm has a pending operation."
msgstr "Tha gnìomh ri dhèiligeadh aig a' chaismeachd."

#: Lomiri/Components/plugin/ucarguments.cpp:188
msgid "Usage: "
msgstr "Cleachdadh: "

#: Lomiri/Components/plugin/ucarguments.cpp:209
msgid "Options:"
msgstr "Roghainnean:"

#: Lomiri/Components/plugin/ucarguments.cpp:498
#, qt-format
msgid "%1 is expecting an additional argument: %2"
msgstr "Tha dùil aig %1 ri argamaid a bharrachd: %2"

#: Lomiri/Components/plugin/ucarguments.cpp:503
#, qt-format
msgid "%1 is expecting a value for argument: %2"
msgstr "Tha dùil aig %1 ri luach airson na h-argamaid: %2"

#: Lomiri/Components/plugin/ucarguments.cpp:520
#, qt-format
msgid "%1 is expecting additional arguments: %2"
msgstr "Tha dùil aig %1 ri argamaidean a bharrachd: %2"

#: Lomiri/Components/plugin/uclistitemstyle.cpp:145
msgid "consider overriding swipeEvent() slot!"
msgstr "tar-àithn an slot swipeEvent() ’s dòcha!"

#: Lomiri/Components/plugin/uclistitemstyle.cpp:165
msgid "consider overriding rebound() slot!"
msgstr "tar-àithne an slot rebound() ’s dòcha!"

#: Lomiri/Components/plugin/ucmousefilters.cpp:1065
msgid "Ignoring AfterItem priority for InverseMouse filters."
msgstr ""
"A' leigeil seachad prìomhachas AfterItem airson criathragan InverseMouse."

#: Lomiri/Components/plugin/ucserviceproperties.cpp:77
msgid "Changing connection parameters forbidden."
msgstr "Tha atharrachadh paramadairean a’ cheangail toirmisgte."

#: Lomiri/Components/plugin/ucserviceproperties.cpp:160
#, qt-format
msgid ""
"Binding detected on property '%1' will be removed by the service updates."
msgstr ""
"Thèid an nasgadh a chaidh a lorg air a’ bhuadh “%1” a thoirt air falbh leis "
"an t-seirbheis ùrachaidh."

#: Lomiri/Components/plugin/ucstatesaver.cpp:46
msgid "Warning: attachee must have an ID. State will not be saved."
msgstr ""
"Rabhadh: Feumaidh ID a bhith aig a' cheangladair. Cha dèid an staid a "
"shàbhaladh."

#: Lomiri/Components/plugin/ucstatesaver.cpp:56
#, qt-format
msgid ""
"Warning: attachee's UUID is already registered, state won't be saved: %1"
msgstr ""
"Rabhadh: Chaidh UUID a' cheangladair a chlàradh mu thràth, cha dèid an staid "
"a shàbhaladh: %1"

#: Lomiri/Components/plugin/ucstatesaver.cpp:107
#, qt-format
msgid ""
"All the parents must have an id.\n"
"State saving disabled for %1, class %2"
msgstr ""
"Feumaidh ID a bhith aig gach pàrant.\n"
"Chaidh sàbhaladh na staid a chur à comas airson %1, clas %2"

#: Lomiri/Components/plugin/uctheme.cpp:208
#, qt-format
msgid "Theme not found: \"%1\""
msgstr "Cha deach an t-ùrlar a lorg: “%1”"

#: Lomiri/Components/plugin/uctheme.cpp:539
msgid "Not a Palette component."
msgstr "Chan e co-phàirt a’ phaileid a tha ann."

#: Lomiri/Components/plugin/ucviewitemsattached.cpp:462
msgid "Dragging mode requires ListView"
msgstr "Feumaidh am modh slaodadh ListView"

#: Lomiri/Components/plugin/ucviewitemsattached.cpp:468
msgid ""
"Dragging is only supported when using a QAbstractItemModel, ListModel or "
"list."
msgstr ""
"Chan eil taic ri slaodadh ach nuair a chleachdar QAbstractItemModel, "
"ListModel no liosta."

#: Lomiri/Components/Popups/1.2/ComposerSheet.qml:78
#: Lomiri/Components/Popups/1.3/ComposerSheet.qml:78
msgid "Cancel"
msgstr "Sguir dheth"

#: Lomiri/Components/Popups/1.2/ComposerSheet.qml:88
#: Lomiri/Components/Popups/1.3/ComposerSheet.qml:88
msgid "Confirm"
msgstr "Dearbhaich"

#: Lomiri/Components/Popups/1.2/DefaultSheet.qml:85
#: Lomiri/Components/Popups/1.3/DefaultSheet.qml:85
msgid "Close"
msgstr "Dùin"

#: Lomiri/Components/Popups/1.2/DefaultSheet.qml:95
#: Lomiri/Components/Popups/1.3/DefaultSheet.qml:95
msgid "Done"
msgstr "Dèanta"

#: Lomiri/Components/Themes/Ambiance/1.2/ProgressBarStyle.qml:51
#: Lomiri/Components/Themes/Ambiance/1.3/ProgressBarStyle.qml:50
msgid "In Progress"
msgstr "A' dol air adhart"

#: Lomiri/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml:28
#: Lomiri/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml:28
msgid "Release to refresh..."
msgstr "Leig às e airson ath-nuadhachadh a dhèanamh..."

#: Lomiri/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml:28
#: Lomiri/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml:28
msgid "Pull to refresh..."
msgstr "Tarraing airson ath-nuadhachadh a dhèanamh..."

#~ msgid "Theme not found: "
#~ msgstr "Cha deach an t-ùrlar a lorg: "
